document.addEventListener("DOMContentLoaded", function () {
    let image = 'https://gitlab.com/someonesilent/uzlabina-waving/raw/master/wave_emote.png';
    document.body.innerHTML += '<div id="mavani" style="' +
        'position:fixed;' +
        'top:50px;' +
        'display:block;' +
        'left:'+(window.innerWidth - 480) / 2+'px;' +
        'width:480px;' +
        'height:475px;' +
        'background-image: url('+image+');' +
        'background-repeat:no-repeat;' +
        'background-size: cover;' +
        '"></div>';
    let node = document.getElementById("mavani");
    node.innerHTML += '<span id="pozdrav" style="' +
        'font-family:Comic Sans MS;' +
        'font-size:38px;' +
        'font-weight:900;' +
        'text-align:center;' +
        'top:200px;' +
        'position:relative;' +
        '">Dobrý den, pane Ryšánku</span>';
    let i = 0;
    let ii = 1;
    let otoceni = true;
    var interval = setInterval(toceni, 15);

    function toceni() {
        node.style.transform = "rotate(" + i + "deg)";
        if (otoceni && i < 90) i++;
        else if (i == 90) {
            otoceni = false;
            i--;
        } else if (!otoceni && i > 0) i--;
        else if (i == 0) {
            otoceni = true;
            i++;
        }
        node.style.opacity = ii;
        ii -= 0.0045;
        if (ii <= 0) {
            node.style.display = "none";
            clearInterval(interval);
        }
    }
});